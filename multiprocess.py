from timeit import timeit


def if_prime(x):
    if x <= 1:
        return 0
    elif x <= 3:
        return x
    elif x % 2 == 0 or x % 3 == 0:
        return 0
    i = 5
    while i ** 2 <= x:
        if x % i == 0 or x % (i + 2) == 0:
            return 0
        i += 6
    return x


def seq():
    answer = 0
    for i in range(10 ** 6):
        answer += if_prime(i)
    print(answer)


def par():
    from multiprocessing import Pool
    with Pool(16) as pool:
        answer = sum(pool.map(if_prime, range(10 ** 6)))
    print(answer)


if __name__ == '__main__':
    t = timeit('seq()', setup='from __main__ import seq', number=1)
    print(f' -- took {t} s')

    t = timeit('par()', setup='from __main__ import par', number=1)
    print(f' -- took {t} s')
