import os

from cv02.algebra import dist
import pandas as ps


def permutation(items):
    return (
        [a] + b
        for a in items
        for b in permutation([
            item
            for item in items
            if item != a
        ])
    ) if items else [[]]


def cyclic_permutation(items):
    return (
        items[:1] + perm
        for perm in permutation(items[1:])
    )


def cycle_dist(nodes):
    return sum([
        dist(a, b)
        for a, b in zip(nodes, nodes[1:] + nodes[:1])
    ])


def node_set(n):
    file = os.path.join(os.path.dirname(__file__), 'a280.tsp')
    data = ps.read_csv(
        file,
        skiprows=6,
        nrows=n,  # Take first N rows
        sep=r'\s+',
        names=('n', 'x', 'y'),
    )
    return list(zip(data.x, data.y))
