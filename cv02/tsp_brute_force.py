import math
from collections import namedtuple

from cv02 import cyclic_permutation, cycle_dist, node_set


def shortest_cycle_step(nodes):
    Step = namedtuple('Step', 'perm, dist, min')
    m = math.inf
    for perm in cyclic_permutation(nodes):
        d = cycle_dist(perm)
        if d < m:
            m = d
        yield Step(perm, cycle_dist(perm), m)


def shortest_cycle(nodes):
    return min(map(lambda s: s.min, shortest_cycle_step(nodes)))


if __name__ == '__main__':
    nodes = node_set(10)
    print(f'Nodes count: {len(nodes)}')

    for perm, d, m in shortest_cycle_step(nodes):
        print(f'cur={d};\tmin={m};\tperm={perm}')
