from multiprocessing.pool import Pool
from os import path
import re
from time import time

import numpy as np


class Book:
    def __init__(self, title):
        self.title = title
        self.words = set()


def index(book, master):
    return np.asarray([
        word in book.words
        for word in master
    ])


if __name__ == '__main__':
    with open(path.join(path.dirname(__file__), 'stopwords.txt')) as words:
        stopwords = {word[:len(word) - 1] for word in words}

    books = []
    with open(path.join(path.dirname(__file__), 'kjb.txt')) as file:
        book = None
        for line in file:
            book_match = re.match(r'^Book \d{2}\s(?P<title>.+)$', line)
            if book_match:
                book = Book(**book_match.groupdict())
                books.append(book)
                continue

            if not book:
                continue

            par_match = re.match(r'^\d{3}:\d{3}\s(?P<par_line>.+)$', line)
            if par_match:
                par_line = par_match.groupdict()['par_line']
                for word in re.findall(r'(\w+)', par_line):
                    n_word = word.lower()
                    if n_word not in stopwords:
                        book.words.add(n_word)

    for book in books:
        print(len(book.words), book.words)

    master = {
        word
        for book in books
        for word in book.words
    }
    master_ord = list(master)
    print(len(master))

    start = time()
    index_seq = np.asmatrix([
        index(book, master_ord)
        for book in books
    ])
    print(index_seq, f'\ntook {time() - start} s')

    start = time()
    with Pool(4) as pool:
        index_par = np.asmatrix(pool.starmap(index, [(book, master_ord) for book in books]))
    print(index_par, f'\ntook {time() - start} s')

    print('equal', np.array_equal(index_seq, index_par))
