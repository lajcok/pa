#![allow(dead_code)]

extern crate rand; // 0.7.3
extern crate crossbeam; // 0.7.3
// crossbeam is not available on Repl.it, run in playground:
// https://play.rust-lang.org/

use std::{fmt, thread, time};
use rand::Rng;

fn animate() {
  let toad: &[&[u8]] = &[
    &[0, 0, 0, 0, 0, 0],
    &[0, 0, 0, 1, 0, 0],
    &[0, 1, 0, 0, 1, 0],
    &[0, 1, 0, 0, 1, 0],
    &[0, 0, 1, 0, 0, 0],
    &[0, 0, 0, 0, 0, 0],
  ];
  let mut gol = GameOfLife::new(toad);
  loop {
    println!("{}", gol);
    gol = gol.step();
    thread::sleep(time::Duration::from_millis(500));
  }
}

fn main() {
    let mut rng = rand::thread_rng();

    let width = 1000;
    let height = 1000;

    let board: Vec<Vec<bool>> = (0..height).map(|_| {
        (0..width).map(|_| {
            rng.gen::<bool>()
        }).collect()
    }).collect();

    let gol = GameOfLife {board, height, width};

    let threads = 4;
    let start = time::Instant::now();
    gol.step_parallel(threads);
    let duration = start.elapsed();

    println!(
        "GameOfLife: {}x{} on {} threads took {:?}",
        width, height, threads, duration,
    );
}


#[derive(Debug)]
struct GameOfLife {
  board: Vec<Vec<bool>>,
  height: usize,
  width: usize,
}

impl fmt::Display for GameOfLife {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", self.board.iter().map(
      |row| row.iter().map(
        |cell| if *cell {"\u{2588}\u{2588}"} else {"\u{2591}\u{2591}"}
      ).collect::<Vec<_>>().concat() + "\n"
    ).collect::<Vec<_>>().concat())
  }
}

impl GameOfLife {
  fn new(board: &[&[u8]]) -> Self {
    Self {
      board: board.iter().map(
        |row| row.iter().map(
          |cell| *cell != 0
        ).collect()
      ).collect(),
      height: board.len(),
      width: board[0].len(),
    }
  }

  fn is_alive(&self, i: usize, j: usize) -> bool {
    self.board[i][j]
  }

  fn shall_live(&self, i: usize, j: usize) -> bool {
    let mut neighbors = 0;
    let is = i as isize;
    let js = j as isize;
    for ii in is-1..=is+1 {
      if ii < 0 || ii >= self.height as isize {
        continue;
      }
      for jj in js-1..=js+1 {
        if jj < 0 || jj >= self.width as isize || (is == ii && js == jj) {
          continue;
        }
        if self.is_alive(ii as usize, jj as usize) {
          neighbors += 1;
        }
      }
    }
    if self.is_alive(i, j) {
      2 <= neighbors && neighbors <= 3
    } else {
      neighbors == 3
    }
  }

  fn step(&self) -> Self {
    let mut board = vec![vec![false; self.width]; self.height];
    for i in 0..self.height {
      for j in 0..self.width {
        board[i][j] = self.shall_live(i, j)
      }
    }
    Self {
      board,
      height: self.height,
      width: self.width,
    }
  }

  fn step_parallel(&self, threads: usize) -> Self {
    let total = self.height * self.width;
    let chunk_size = total / threads;

    let mut board = vec![vec![false; self.width]; self.height];

    // Flattened mutable board for convenient chunking
    let mut board_flat: Vec<(usize, usize, &mut bool)> = board
        .iter_mut()
        .enumerate()
        .flat_map(
            |(i, row)| row
                .iter_mut()
                .enumerate()
                .map(move |(j, cell)| (i, j, cell))
        )
        .collect();

    // Spawn scoped processes over chunks
    let _ = crossbeam::scope(|scope| {
        for chunk_slice in board_flat.chunks_mut(chunk_size) {
            scope.spawn(move |_| {
                for (i, j, cell) in chunk_slice {
                    **cell = self.shall_live(*i, *j);
                }
            });
        }
    });

    Self {
      board,
      height: self.height,
      width: self.width,
    }
  }

}
