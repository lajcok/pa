package main

import (
  "fmt"
  "time"
  "math/rand"
)

type GameOfLife struct {
  board [][]bool
  height, width int
}

func NewGameOfLife(board [][]int) (gol *GameOfLife) {
  gol = &GameOfLife{
    make([][]bool, len(board)),
    len(board), 0,
  }
  for i := range board {
    gol.board[i] = make([]bool, len(board[i]))
    gol.width = len(board[i])
    for j := range board[i] {
      gol.board[i][j] = board[i][j] != 0
    }
  }
  return
}

func (g *GameOfLife) String() (res string) {
  for i := range g.board {
    for j := range g.board[i] {
      if g.board[i][j] {
        res += "\u2588\u2588"
      } else {
        res += "\u2591\u2591"
      }
    }
    res += "\n"
  }
  return
}

func (g *GameOfLife) IsAlive(i, j int) bool {
  return g.board[i][j]
}

func (g *GameOfLife) ShallLive(i, j int) bool {
  var neighbors int
  for ii := i - 1; ii <= i + 1; ii++ {
    if ii < 0 || ii >= g.height {
      continue
    }
    for jj := j - 1; jj <= j + 1; jj++ {
      if jj < 0 || jj >= g.width || (i == ii && j == jj) {
        continue
      }
      if g.IsAlive(ii, jj) {
        neighbors++
      }
    }
  }
  if g.IsAlive(i, j) {
    return 2 <= neighbors && neighbors <= 3
  } else {
    return neighbors == 3
  }
}

func (g *GameOfLife) Step() *GameOfLife {
  b := make([][]bool, g.height)
  for i := range g.board {
    b[i] = make([]bool, g.width)
    for j := range g.board[i] {
      b[i][j] = g.ShallLive(i, j)
    }
  }
  return &GameOfLife{b, g.height, g.width}
}

func (g *GameOfLife) StepParallel(routines int) *GameOfLife {
  b := make([][]bool, g.height)
  cs, cIdx := make([]chan [2]int, routines), 0
  for i := range g.board {
    b[i] = make([]bool, g.width)
    for j := range g.board[i] {
      if cs[cIdx] == nil {
        cs[cIdx] = make(chan [2]int)
        go func(coords chan [2]int) {
          for {
            coord, open := <- coords
            if !open {
              return
            }
            b[coord[0]][coord[1]] = g.ShallLive(coord[0], coord[1])
          }
        }(cs[cIdx])
      }
      cs[cIdx] <- [2]int{i, j}
      cIdx = (cIdx + 1) % len(cs)
    }
  }
  for _, c := range cs {
    close(c)
  }
  return &GameOfLife{b, g.height, g.width}
}

func animate() {
  gol := NewGameOfLife([][]int{
    {0, 0, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0},
    {0, 1, 0, 0, 1, 0},
    {0, 1, 0, 0, 1, 0},
    {0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0},
  })
  for step := gol;; step = step.Step() {
    fmt.Println(step)
    time.Sleep(500 * time.Millisecond)
  }
}

func benchmark() {
  h, w := 1000, 1000
  gol := GameOfLife{
    make([][]bool, h),
    h, w,
  }
  for i := range gol.board {
    gol.board[i] = make([]bool, w)
    for j := range gol.board[i] {
      gol.board[i][j] = rand.Intn(2) != 0
    }
  }

  start := time.Now()
  gol.Step()
  elapsed := time.Since(start)
  fmt.Printf("Time elapsed: %s\n", elapsed)
}

func double(x int) int {
  return 2 * x
}

func main() {
  //animate()
  //benchmark()

  n := 100000

  numbers := make([]int, n)
  for i := range numbers {
    numbers[i] = i
  }
  fmt.Println(numbers[:10])

  doubles := make([]int, n)
  start := time.Now()
  for i, x := range numbers {
    //doubles[i] = double(x)

    func (ii, xx int) {
      doubles[ii] = double(xx)
    }(i, x)

  }
  elapsed := time.Since(start)
  fmt.Printf("Time elapsed: %s\n", elapsed)

  fmt.Println(doubles[:10])
}
