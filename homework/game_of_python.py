import numpy as np


class GameOfLife:

    # Initialization

    def __new__(cls, board, parallel=None):
        return object.__new__(GameOfLifeParallel if parallel else GameOfLifeSequential)

    def __init__(self, board, *args, **kwargs):
        """
        :param board: Initial board
        :param parallel: If truthful, parallel iteration will be used or a custom number of processes can be specified.
                            Otherwise sequential algorithm will be used.
        """
        self.board = np.asarray(board, dtype=bool)

    # Interface

    def height(self):
        return self.board.shape[0]

    def width(self):
        return self.board.shape[1]

    def is_alive(self, i, j):
        return 0 <= i < self.width() and 0 <= j < self.height() and self.board[i, j]

    def shall_live(self, i, j):
        neighbors = np.sum(self.board[(i - 1):(i + 2), (j - 1):(j + 2)])
        return 2 <= neighbors - 1 <= 3 if self.is_alive(i, j) else neighbors == 3

    # Stepping

    def step(self):
        raise NotImplementedError

    # Magic

    def __iter__(self):
        current = self
        yield current
        while current.board.any():
            current = current.step()
            yield current

    def __getitem__(self, position):
        return self.is_alive(*position)

    def __str__(self):
        return '\n'.join([
            ''.join([
                '\u2588\u2588' if cell else '\u2591\u2591'
                for cell in row
            ])
            for row in self.board
        ]) + '\n'


class GameOfLifeSequential(GameOfLife):

    # Initialization

    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)

    # Stepping

    def step(self):
        board = np.empty_like(self.board)
        for i in range(self.height()):
            for j in range(self.width()):
                board[i, j] = self.shall_live(i, j)
        return self.__class__(board)


class GameOfLifeParallel(GameOfLife):

    # Initialization

    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)

    def __init__(self, board, parallel):
        super().__init__(board)
        self._parallel = parallel

    # Stepping

    def step(self):
        from multiprocessing import Pool
        pool = Pool(self._parallel if isinstance(self._parallel, int) and self._parallel > 0 else None)
        next_state = pool.starmap(
            func=self.shall_live,
            iterable=np.ndindex(self.board.shape),
        )
        pool.close()
        pool.join()
        return GameOfLife(
            board=np.asarray(next_state).reshape(self.board.shape)
        )


def animate(game_of_life, fps=2):
    from time import sleep
    t = fps ** -1
    for g in game_of_life:
        print(g)
        sleep(t)


def benchmark(game_of_life, iterations):
    from timeit import timeit
    elapsed = timeit(game_of_life.step, number=iterations + 1)
    print(f'{iterations} iterations of GOL[{game_of_life.height()}×{game_of_life.width()}] took {elapsed} sec.')
    return elapsed


if __name__ == '__main__':
    print('Benchmark:')
    board = np.empty((100,) * 2, dtype=bool)
    gol = GameOfLife(
        board,
        # parallel=False,   # ~ 10.2953100 sec
        # parallel=2,       # ~  7.5576956 sec
        parallel=3,  # ~  6.2499422 sec
    )
    benchmark(gol, int(1))

    print('Animation:')
    toad = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 1, 0],
        [0, 1, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
    ]
    gol = GameOfLife(toad)
    animate(gol)
