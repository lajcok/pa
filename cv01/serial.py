from time import time

import requests


def get(i):
    r = requests.get(f'http://name-service.appspot.com/api/v1/names/{i}.json')
    return r.json() if r.status_code == 200 else None


if __name__ == '__main__':
    n = 100

    print('Starting requests...')
    start = time()

    for x in range(n):
        response = get(x)
        print(f'{x}: {response}')

    print('Stopping requests...')
    print(f'Obtained {n} records in {time() - start} seconds')
