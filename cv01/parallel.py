from threading import Thread
from time import time

import requests


def get(i):
    r = requests.get(f'http://name-service.appspot.com/api/v1/names/{i}.json')
    return r.json() if r.status_code == 200 else None


def job(begin, end, step):
    for x in range(begin, end, step):
        response = get(x)
        if not response:
            break

        print(f'{x}: {response}')


if __name__ == '__main__':
    n = 4000
    thread_n = 10

    threads = [
        Thread(target=job, args=(i, n, thread_n))
        for i in range(thread_n)
    ]

    print('Starting requests...')
    start = time()

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    print('Stopping requests...')
    print(f'Obtained {n} records in {time() - start} seconds')
