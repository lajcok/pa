import math
from collections import namedtuple
from random import random
from time import time

Point = namedtuple('Point', 'x, y')


def count(it, cond=None):
    c = 0
    for x in it:
        if not cond or cond(x):
            c += 1
    return c


def rand_point():
    return Point(random(), random())


def in_circle(point: Point):
    return point.x ** 2 + point.y ** 2 <= 1


def approx_pi(n_samples):
    return 4 * count(
        (rand_point() for _ in range(n_samples)),
        lambda p: in_circle(p)
    ) / n_samples


def seq(n):
    return approx_pi(n)


def par(n, p):
    from multiprocessing import Pool
    rel = math.ceil(n / p)
    with Pool(p) as pool:
        res = pool.map(approx_pi, (rel for _ in range(p)))
    return sum(res) / len(res)


pi_ref = '3.1414159265358979323846264338327950288419'


def accuracy(pi):
    c = -2
    for p, r in zip(str(pi), pi_ref):
        if p == r:
            c += 1
        else:
            break
    return c


if __name__ == '__main__':
    n = 10 ** 7
    p = 16
    print(f'samples = {n}; processes = {p}')
    print(f'ref \u03C0 value = {pi_ref}')

    start = time()
    pi = seq(n)
    dur = time() - start
    print(f'[seq] approx \u03C0 = {seq(n)}; took {dur} s; accuracy = {accuracy(pi)}')

    start = time()
    pi = par(n, p)
    dur = time() - start
    print(f'[par] approx \u03C0 = {pi}; took {dur} s; accuracy = {accuracy(pi)}')

